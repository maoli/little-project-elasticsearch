package com.ruyuan.little.project.elasticsearch.biz.admin.dto;

import lombok.Data;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:后台商品查询dto组件
 **/
@Data
public class AdminGoodsSpuDTO {

    /**
     * 店铺id
     */
    private String storeId;

    /**
     * 商品名称
     */
    private String  goodsName;

    /**
     * 页码
     */
    private Integer page;

    /**
     * 每页大小
     */
    private Integer size;

}
