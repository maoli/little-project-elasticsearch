package com.ruyuan.little.project.elasticsearch.biz.admin.dto;

import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSku;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSpu;
import lombok.Data;

import java.util.List;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
@Data
public class AdminGoodsSkuAddDTO {

    /**
     * 店铺id
     */
    private String storeId;

    /**
     * 商品spu
     */
    private AdminGoodsSpu adminGoodsSpu;

    /**
     * 商品sku
     */
    private List<AdminGoodsSku> adminGoodsSkuList;
}
